exports.order = function order(req, res, next) {
  tableCode = new Array("DE","UK","FR","IT","ES","PL","RO","NL","BE","EL","CZ","PT","HU","SE","AT","BG","DK","FI","SK","IE","HR","LT","SI","LV","EE","CY","LU","MT");
  tableTaxe= new Array(0.20,0.21,4,0.25,0.19,0.21,0.20,0.20,0.24,0.20,0.19,0.23,0.27,0.23,0.22,0.21,0.21,0.17,0.18,0.21,0.23,0.23,0.24,0.20,0.22,0.21,0.25,0.20);

  position = tableCode.indexOf(req.body.country);
  if (position == -1 ) {
    res.status(400);
    return;
  }

  taxe = tableTaxe[position];
  console.log(req.body)

  if (req.status == 404){
    res.status(400);
  }

  if (req.body.quantities.length != req.body.prices.length){
    res.status(400);
  }

  if(Array.isArray(req.body.quantities) == false){
    res.status(400);
  }

  var total = 0
  if (req.body.prices == null || req.body.quantities == null || req.body.country == null || req.body.reduction == null){
    res.status(400);
  }

  for (i=0; i<req.body.prices.length; i++){
    total += req.body.prices[i] * req.body.quantities[i]
  }

  if (req.body.country == 'SK'){
    if (total > 2000){
      taxe = 0;
    }
    else {
      taxe = 0.18;
    }
  }
  else if (req.body.country == 'BE'){
    if (total > 2000){
      taxe = 0.25;
    }
    else {
      taxe = 0;
    }
  }


  total = total + (total * taxe);

  if (req.body.reduction == 'HALF PRICE') {
    total = total/2;
  }
  else if (req.body.reduction == 'STANDARD') {
    if (total >= 1000 && total < 5000) {
        total = total * 0.97;
    }
    else if (total >= 5000 && total < 7000 ){
        total = total * 0.95;
    }
    else if (total >= 7000 && total < 10000){
        total = total * 0.93;
    }
    else if (total >= 10000 && total < 50000){
        total = total * 0.90;
    }
    else if (total >= 50000){
        total = total * 0.85;
    }
  }
  else if (req.body.reduction == 'PAY THE PRICE') {
    
  }
  else {
      res.json({});
  }

  if (req.body.country == 'UK'){
    total = total - (total*0.25);
  }
  console.log(total);
  //res.json({});
  res.json({total});
}

exports.feedback = function feedback(req, res, next) {
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
}
