exports.order = function order(req, res, next) {
  // TODO implement from here
  
  var total = 0
  var taxe = {
    DE: "0.20",
    UK: "0.21",
    FR: "0.20",
    IT: "0.25",
    ES: "0.19",
    PL: "0.21",
    RO: "0.20",
    NL: "0.20",
    BE: "0.24",
    EL: "0.20",
    CZ: "0.19",
    PT: "0.23",
    HU: "0.27",
    SE: "0.23",
    AT: "0.22",
    BG: "0.21",
    DK: "0.21",
    FI: "0.17",
    SK: "0.18",
    IE: "0.21",
    HR: "0.23",
    LT: "0.23",
    SI: "0.24",
    LV: "0.20",
    EE: "0.22",
    CY: "0.21",
    LU: "0.25",
    MT: "0.20"
  };


  if (!(req.body.country in taxe)) {
    res.status(400).end();
    return;
  }

  taxe = taxe[req.body.country];
  console.log(req.body)

  if (req.status == 404){
    res.status(400).end();
    return;
  }

  if (req.body.quantities.length != req.body.prices.length){
    res.status(400).end();
    return;
  }

  if(Array.isArray(req.body.quantities) == false){
    res.status(400).end();
    return;
  }

  var total = 0
  if (req.body.prices == null || req.body.quantities == null || req.body.country == null || req.body.reduction == null){
    res.status(400).end();
    return;
  }

  for (i=0; i<req.body.prices.length; i++){
    total += req.body.prices[i] * req.body.quantities[i]
  }

  /*if (req.body.country == 'SK'){
    if (total > 2000){
      taxe = 0;
    }
    else {
      taxe = 0.18;
    }
  }
  else if (req.body.country == 'BE'){
    if (total > 2000){
      taxe = 0.25;
    }
    else {
      taxe = 0;
    }
  }*/


  total = total + (total * taxe);

  if (req.body.reduction == 'HALF PRICE') {
    total = total/2;
  }
  else if (req.body.reduction == 'STANDARD') {
    if (total >= 1000 && total < 5000) {
        total = total * 0.97;
    }
    else if (total >= 5000 && total < 7000 ){
        total = total * 0.95;
    }
    else if (total >= 7000 && total < 10000){
        total = total * 0.93;
    }
    else if (total >= 10000 && total < 50000){
        total = total * 0.90;
    }
    else if (total >= 50000){
        total = total * 0.85;
    }
  }
  else if (req.body.reduction == 'PAY THE PRICE') {
    
  }
  else {
      var vide = {}
      res.json({vide});
  }
/*
  if (req.body.country == 'UK'){
    total = total - (total*0.25);
  }*/
  console.log(total);
  res.json({total});

  

};

exports.feedback = function feedback(req, res, next) {
  console.info("FEEDBACK:", req.body.type, req.body.content);
  next();
};
